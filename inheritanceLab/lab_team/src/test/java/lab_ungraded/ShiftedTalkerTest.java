package lab_ungraded;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class ShiftedTalkerTest {
    @Test
    public void ShiftedTalker(){
        ShiftedTalker b = new ShiftedTalker(3);
        assertEquals("defabc", b.talk("abcdef"));
    }
}

