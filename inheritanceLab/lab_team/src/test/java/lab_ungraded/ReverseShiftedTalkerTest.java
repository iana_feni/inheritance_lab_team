package lab_ungraded;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class ReverseShiftedTalkerTest {
    @Test
    public void ReverseShiftedTest(){
        ReversedTalker b = new ReversedTalker(3);
        assertEquals("dcbafe", b.talk("abcdef"));
    }
}
