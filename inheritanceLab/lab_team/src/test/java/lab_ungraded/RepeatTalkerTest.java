package lab_ungraded;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
public class RepeatTalkerTest {
    
    @Test
    public void RepeatTest(){
        RepeatTalker r = new RepeatTalker(2);
        assertEquals("hello hello ", r.talk("hello"));
    }
}
