package lab_ungraded;

public class ShiftedTalker extends Talker {
   private int number;
 

    public ShiftedTalker(int number){
        this.number=number;
    }

    public String talk(String word){
        if(word.length()<this.number){
            throw new IllegalArgumentException("The string you privided has a length smaller than the number you requested!");
        }
        String print="";
        for(int i=this.number;i<word.length();i++)
        {
            print+=word.charAt(i);
        }
        for(int i=0;i<this.number;i++)
        {
            print+=word.charAt(i);
        }
        return print;
    }
    

}
