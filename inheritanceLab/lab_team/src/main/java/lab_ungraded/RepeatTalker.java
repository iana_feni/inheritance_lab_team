package lab_ungraded;

public class RepeatTalker extends Talker {
    private int repeats;
    public RepeatTalker(int n){
        this.repeats=n;
    }
    public String talk(String message){
        String s="";
        for (int i=0;i<this.repeats;i++){
            s+=message +" ";
        }
        return s;
    }
    
}
