package lab_ungraded;

public class ReversedTalker {
    private int number;
    public ReversedTalker(int number){
        this.number=number;
    }
    public String talk(String message){
        if(message.length()<this.number){
            throw new IllegalArgumentException("The string you privided has a length smaller than the number you requested!");
        }
        String result = "";
        for (int i = this.number; i>=0;i--){
            result+=message.charAt(i);
        }
        for (int i = message.length() - 1; i >= this.number+1; i--) {
            result += message.charAt(i);
        }
        return result;
    }


}
